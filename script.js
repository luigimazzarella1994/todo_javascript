function toDoList(){
    let ultodo, input, btnall,btnuncompleted,btncompleted;
    let todos = [];

    const loadTodosFromLocalStorage = () =>{
        const localTodos = localStorage.getItem('todos');
        if(localTodos){
            const todoArr= JSON.parse(localTodos);
            if(todoArr){
                todos = todoArr;
            }
        }
    };

    const saveTodoToLocalStorage = ()=>{
        localStorage.setItem('todos', JSON.stringify(todos));
    };

const removeTodo = (id) =>{
    todos =todos.filter(todo => todos.id !== id);
    saveTodoToLocalStorage();
    ultodo.removeChild(ultodo.querySelector('#todo-'+id));
   
};
 
const addListeners = ()=>{
    btnall= document.querySelector('#btnall');
    btnuncompleted = document.querySelector('#btnuncompleted');
    btncompleted = document.querySelector('#btncompleted');

  btnall.addEventListener('click', (e) =>{
    
      e.target.classList.toggle('active');
      btncompleted.classList.remove('active');
      btnuncompleted.classList.remove('active');
          renderTodos('all');
  });
  btncompleted.addEventListener('click',e =>{
      e.target.classList.toggle('active');
     btnall.classList.remove('active');
      btnuncompleted.classList.remove('active');
      renderTodos('completed');
   });
  btnuncompleted.addEventListener('click',e =>{
     
      e.target.classList.toggle('active');
     btncompleted.classList.remove('active');
      btnall.classList.remove('active');
      renderTodos('uncompleted');
   });
 };

const toggleTodo= (id, ele)=>{
    todos =todos.map(ele=>{
        if(ele.id=== id){
            ele.completed= !ele.completed;
        }
        return ele;
    });
    saveTodoToLocalStorage();

    const oldClass = ele.classList.contains('completed') ? 'completed' :'uncompleted';

    const newClass = oldClass === 'completed' ? 'uncompleted': 'completed';

    ele.classList.replace(oldClass, newClass);

    ele.parentNode.classList.toggle('completed');
 };

 const createLi = ({text,id,completed}) => {
        /*
        <li class="completed">
        <span class= "completed"></span>  
        todo1
        <span class= "cross"></span>  
        </li> */
        const li = document.createElement('li');
        li.id='todo-' +id;
        if(completed){
            li.classList.add('completed');
        }
        const spancheck = document.createElement('span');
        spancheck.classList.add(completed ? 'completed' : 'uncompleted');
        spancheck.addEventListener('click', e =>{
            toggleTodo(id, e.target);
        });
       const spancross = document.createElement('span');
       spancross.classList.add('cross');
       spancross.addEventListener('click', (e)=>{
           removeTodo(id);
       });
        const textNode = document.createTextNode(text);
        li.appendChild(spancheck);
        li.appendChild(textNode);
        li.appendChild(spancross);
        return li;
    };

    const addNewTodo = (todo) => {
        todos.unshift(todo);
        saveTodoToLocalStorage();
        const li = createLi(todo);
        const firstLi = ultodo.firstChild;
        if(!firstLi){
            ultodo.appendChild(li);
        } else{
            ultodo.insertBefore(li, firstLi);
        }
    

    }

    const addTodo = (e) =>{
        const key = e.keyCode;
        const ele = e.target;
        if(key === 13 && ele.value.trim().length > 2){
            const todo ={
                text: ele.value.trim(),
                id: todos.length,
                completed: false,

            }

            addNewTodo(todo);
            ele.value='';
        }
        
    }

    const renderTodos = (todoType) =>{
       
        const lis = ultodo.querySelectorAll('li');
        if(lis){
            lis.forEach(li => ultodo.removeChild(li));
        }
        const currentTodos= todos.filter(todo =>{
            if(todoType==='all'){
                return todo;
            }
            return (todoType=== 'completed') ? todo.completed : !todo.completed;
        } );

        currentTodos.map((todo) => createLi(todo))
         .forEach(li => ultodo.appendChild(li));
    }
    const renderTodosList =() =>{
        loadTodosFromLocalStorage();
     ultodo = document.querySelector('ul#todolist');
        if(!ultodo){
            ultodo = document.createElement('ul');
            ultodo.id = 'todolist';
            document.body.appendChild(ultodo);
        }
        
        renderTodos('all');


      input = document.querySelector('#todo');
         if(!input){
             input= document.createElement('input');
             input.id= 'todo';
             input.name = 'todo';
             input.placeholder = 'Add new todo';
             ultodo.parentNode.insertBefore(input, ultodo);
         }

         input.addEventListener('keyup', addTodo);
         addListeners();
      
     
    };

    return {
        getTodos : function(){
         return todos;
     },
         init : function(){
             renderTodosList();
     }
    }

}

const myTodo = toDoList();
myTodo.init();

